class CertificateUploader < BaseUploader
  version :thumb do
    process resize_to_fit: [278, 278]
  end

  version :featured do
    process resize_to_fit: [800, 600]
  end
end
