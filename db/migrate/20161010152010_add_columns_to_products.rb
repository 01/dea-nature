class AddColumnsToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :nutritional_value, :string
    add_column :products, :energy_value, :string
    add_column :products, :weight, :string
  end
end
