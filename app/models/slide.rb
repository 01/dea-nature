class Slide < ActiveRecord::Base
  validates :image, presence: true
  
  mount_uploader :image, SlideUploader

  scope :sorted, -> {
    order(:position)
  }
end
