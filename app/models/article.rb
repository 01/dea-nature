class Article < ActiveRecord::Base
  mount_uploader :image, ArticleUploader

  validates :title, :subtitle, :content, :image, presence: true

  scope :sorted, -> { order(created_at: :desc) }
end
