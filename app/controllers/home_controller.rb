class HomeController < ApplicationController
  def index
    @slides = Slide.sorted
    @products = Product.sorted.limit(3)
    @articles = Article.sorted.limit(3)
  end
end
