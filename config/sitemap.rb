# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.deanature.com"

SitemapGenerator::Sitemap.create do
  add products_path, priority: 0.9, changefreq: "daily"
  add articles_path, priority: 0.7, changefreq: "daily"

  Product.find_each do |product|
    add product_path(product), lastmod: product.updated_at
  end

  Article.find_each do |article|
    add article_path(article), lastmod: article.updated_at
  end
end
