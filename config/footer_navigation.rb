# -*- coding: utf-8 -*-
# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  navigation.autogenerate_item_ids = false

  navigation.items do |primary|
    primary.dom_class = "footer-nav"

    primary.item :products,
      "Продукция",
      products_path,
      html: { class: 'footer-nav__item' },
      link_html: { class: 'footer-nav-link' }

    primary.item :articles,
      "Статьи",
      articles_path,
      html: { class: 'footer-nav__item' },
      link_html: { class: 'footer-nav-link' }

    primary.item :certificates,
      "Сертификаты",
      certificates_path,
      html: { class: 'footer-nav__item' },
      link_html: { class: 'footer-nav-link' }

    primary.item :pages,
      "О нас",
      page_by_permalink_path("about"),
      html: { class: 'footer-nav__item' },
      link_html: { class: 'footer-nav-link' }

    primary.item :pages,
      "Контакты",
      page_by_permalink_path("contacts"),
      html: { class: 'footer-nav__item' },
      link_html: { class: 'footer-nav-link' }
  end
end
