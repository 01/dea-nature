class AddPositionToSlides < ActiveRecord::Migration[5.0]
  def change
    add_column :slides, :position, :integer
  end
end
