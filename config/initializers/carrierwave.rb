# -*- encoding : utf-8 -*-
if Rails.env.production?
  # -*- encoding : utf-8 -*-
  CarrierWave.configure do |config|
    config.fog_provider = 'fog/google'
    config.fog_credentials = {
      provider:                         'Google',
      google_storage_access_key_id:     Rails.application.secrets.google_storage_key,
      google_storage_secret_access_key: Rails.application.secrets.google_storage_secret
    }
    config.fog_directory = Rails.application.secrets.google_storage_dir
  end
end
