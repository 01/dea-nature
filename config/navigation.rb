# -*- coding: utf-8 -*-
# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  navigation.autogenerate_item_ids = false
  navigation.selected_class = 'main-nav__item--active'

  navigation.items do |primary|
    primary.dom_class = "main-nav"

    primary.item :products,
      "Продукция",
      products_path,
      html: { class: 'main-nav__item' },
      link_html: { class: 'main-nav-link' },
      highlights_on: :subpath

    primary.item :articles,
      "Статьи",
      articles_path,
      html: { class: 'main-nav__item' },
      link_html: { class: 'main-nav-link' },
      highlights_on: :subpath

    primary.item :certificates,
      "Сертификаты",
      certificates_path,
      html: { class: 'main-nav__item' },
      link_html: { class: 'main-nav-link' },
      highlights_on: :subpath

    primary.item :pages,
      "О нас",
      page_by_permalink_path("about"),
      html: { class: 'main-nav__item' },
      link_html: { class: 'main-nav-link' },
      highlights_on: :subpath

    primary.item :pages,
      "Контакты",
      page_by_permalink_path("contacts"),
      html: { class: 'main-nav__item' },
      link_html: { class: 'main-nav-link' },
      highlights_on: :subpath
  end
end
