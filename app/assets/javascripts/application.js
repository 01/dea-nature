//= require jquery
//= require jquery_ujs
//= require tether
//= require bootstrap
//= require swiper
//= require_tree .

$('.slider').each(function () {
  var targetEl = $(this).find('.swiper-container');
  var nextEl = $(this).find('.slider__arrow--next');
  var prevEl = $(this).find('.slider__arrow--prev');

  new Swiper(targetEl, {
    // Disable preloading of all images
    preloadImages: false,
    // Enable lazy loading
    lazyLoading: true,

    loop: true,
    slidesPerView: 1,
    spaceBetween: 30,

    // Navigation arrows
    nextButton: nextEl,
    prevButton: prevEl,

    // Autoplay
    autoplay: 5000
  });
})
