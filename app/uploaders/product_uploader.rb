class ProductUploader < BaseUploader
  version :thumb do
    process resize_to_fill: [278, 278]
  end

  version :featured do
    process resize_to_fill: [800, 800]
  end
end
