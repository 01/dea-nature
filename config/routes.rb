Rails.application.routes.draw do
  
  #root "home#index"
  
  resources :pages
  get "/info/:permalink", to: "pages#show", as: :page_by_permalink
  resources :certificates
  resources :articles
  resources :home
  resources :products

  namespace :manage do
    root to: "products#index"
    get "/login" => "auth#new"
    post "/auth" => "auth#check"
    delete "/logout" => "auth#logout"

    resource :settings, only: [:edit, :update]
    resources :pages
    resources :certificates
    resources :articles
    resources :products
    resources :slides
  end
  root 'home#index'
end
