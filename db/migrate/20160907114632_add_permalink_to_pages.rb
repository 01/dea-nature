class AddPermalinkToPages < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :permalink, :string
  end
end
