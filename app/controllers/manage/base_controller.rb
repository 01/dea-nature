class Manage::BaseController < ApplicationController
  layout "manage"
  before_action :verify_admin

  private

    def verify_admin
      redirect_to manage_login_path unless current_admin
    end
end
