SimpleNavigation::Configuration.run do |navigation|
  navigation.autogenerate_item_ids = false
  navigation.selected_class = 'active'

  navigation.items do |primary|
    primary.dom_class = "nav nav-tabs admin-nav"

    primary.item :products,
      "Продукция",
      manage_products_path,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' },
      highlights_on: :subpath

    primary.item :certificates,
      "Сертификаты",
      manage_certificates_path,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' },
      highlights_on: :subpath

    primary.item :slides,
      "Слайды",
      manage_slides_path,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' },
      highlights_on: :subpath

    primary.item :articles,
      "Статьи",
      manage_articles_path,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' },
      highlights_on: :subpath

    primary.item :pages,
      "Страницы",
      manage_pages_path,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' },
      highlights_on: :subpath

    primary.item :settings,
      "Настройки",
      edit_manage_settings_path,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' },
      highlights_on: :subpath

    primary.item :logout,
      "Выход",
      manage_logout_path,
      method: :delete,
      html: { class: 'nav-item' },
      link_html: { class: 'nav-link' }

  end
end
