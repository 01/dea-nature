class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :settings, :current_admin

  private

    def settings
      @settings ||= (Setting.last || Setting.create)
    end

    def current_admin
      @current_admin ||= Admin.where(id: session[:admin_id]).first if session[:admin_id]
    end
end
