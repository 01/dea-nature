class Admin < ActiveRecord::Base
  has_secure_password

  def self.god
    where(login: Rails.application.secrets.admin_login).first
  end
end
