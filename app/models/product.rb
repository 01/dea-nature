class Product < ActiveRecord::Base
  mount_uploader :image, ProductUploader

  validates :title, :description, :image, presence: true

  scope :sorted, -> { order(created_at: :desc) }
end
