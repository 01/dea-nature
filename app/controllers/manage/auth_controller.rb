class Manage::AuthController < Manage::BaseController
  skip_before_filter :verify_admin, except: :logout

  def new
    redirect_to manage_root_path if current_admin
  end
  
  def check
    admin = Admin.god
    if admin && admin.authenticate(params[:auth][:password])
      session[:admin_id] = admin.id
      redirect_to manage_root_path
    else
      redirect_to :back
    end
  end

  def logout
    session[:admin_id] = nil
    redirect_to manage_login_path
  end
end