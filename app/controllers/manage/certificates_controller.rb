class Manage::CertificatesController < Manage::BaseController
  before_action :set_certificate, only: [:show, :edit, :update, :destroy]

  # GET manage/certificates
  # GET manage/certificates.json
  def index
    @certificates = Certificate.all
  end

  # GET manage/certificates/new
  def new
    @certificate = Certificate.new
  end

  # GET manage/certificates/1/edit
  def edit
  end

  # POST manage/certificates
  # POST manage/certificates.json
  def create
    @certificate = Certificate.new(certificate_params)

    respond_to do |format|
      if @certificate.save
        format.html { redirect_to manage_certificates_path, notice: 'Certificate was successfully created.' }
        format.json { render :show, status: :created, location: @certificate }
      else
        format.html { render :new }
        format.json { render json: @certificate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT manage/certificates/1
  # PATCH/PUT manage/certificates/1.json
  def update
    respond_to do |format|
      if @certificate.update(certificate_params)
        format.html { redirect_to manage_certificates_path, notice: 'Certificate was successfully updated.' }
        format.json { render :show, status: :ok, location: @certificate }
      else
        format.html { render :edit }
        format.json { render json: @certificate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE manage/certificates/1
  # DELETE manage/certificates/1.json
  def destroy
    @certificate.destroy
    respond_to do |format|
      format.html { redirect_to manage_certificates_path, notice: 'Certificate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_certificate
      @certificate = Certificate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def certificate_params
      params.require(:certificate).permit!
    end
end
