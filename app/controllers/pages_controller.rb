class PagesController < ApplicationController
  def show
    @page = if params[:permalink]
      Page.find_by(permalink: params[:permalink])
    else
      Page.find(params[:id])
    end
  end
end
