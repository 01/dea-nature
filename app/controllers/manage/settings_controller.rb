class Manage::SettingsController < Manage::BaseController
  def edit
  end

  def update
    if settings.update_attributes(settings_params)
      redirect_to manage_root_path
    else
      render :edit
    end
  end

  private

    def settings_params
      params.require(:setting).permit!
    end
end
