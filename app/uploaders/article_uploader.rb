class ArticleUploader < BaseUploader
  version :thumb do
    process resize_to_fill: [250, 278]
  end

  version :featured do
    process resize_to_fill: [800, 600]
  end
end