class CreateSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :settings do |t|
      t.text :address
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
